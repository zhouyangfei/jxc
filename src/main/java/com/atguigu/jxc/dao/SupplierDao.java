package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupplierDao {

    List<Supplier> listByPage(@Param("page") Integer page,
                              @Param("rows") Integer rows,
                              @Param("supplierName") String supplierName);

//    Integer save( @Param("supplierName") String supplierName,
//              @Param("contacts") String contacts,
//              @Param("phoneNumber") String phoneNumber,
//              @Param("address") String address,
//              @Param("remarks") String remarks);
      Integer save(Supplier supplier);

    Integer delete(List list);

    Integer update(Supplier supplier);
}
