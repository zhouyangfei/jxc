package com.atguigu.jxc.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * 商品类别
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsType {

  private Integer goodsTypeId;
  private String text;
  private Integer pId;
  private Integer goodsTypeState;
  private String iconCls="goods-type";
  private String state;
  private Map<String,Object> attributes;
  private List<GoodsType> children;


}
