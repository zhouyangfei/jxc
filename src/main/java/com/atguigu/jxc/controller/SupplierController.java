package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/supplier")
public class SupplierController {
    @Autowired
    private SupplierService supplierService;

    @PostMapping("/list")
    public Map<String,Object>list(Integer page, Integer rows, String supplierName){
        return supplierService.listByPage(page,rows,supplierName);
    }
    //添加或修改
    @PostMapping("/save")
    public ServiceVO save( Supplier supplier){

        Integer save = supplierService.save(supplier);
        if(save>0){
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }else {
            return new ServiceVO(ErrorCode.SAVE_ERROR_CODE,ErrorCode.SAVE_ERROR_MESS);
        }

    }
    @PostMapping("/delete")
    public ServiceVO delete(String  ids){
        return   supplierService.delete(ids);
    }
}
