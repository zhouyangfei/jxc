package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {
    Map<String, Object> listByPage(Integer page, Integer rows, String supplierName);

    Integer save(Supplier supplier);

    ServiceVO delete(String ids);
}
