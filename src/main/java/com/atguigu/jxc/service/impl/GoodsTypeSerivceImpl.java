package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.service.GoodsTypeSerivce;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class GoodsTypeSerivceImpl implements GoodsTypeSerivce {
    @Autowired
    private GoodsTypeDao goodsTypeDao;
    @Override
    public List<GoodsType> loadGoodsType() {
      //获取所有的一级目录
        GoodsType goodsTypeExam = new GoodsType();
        goodsTypeExam.setGoodsTypeId(-1);
        List<GoodsType> goodsTypeList = goodsTypeDao.getGoodsTypeList(goodsTypeExam);

       //获取一级目录下的所有目录
       if(!CollectionUtils.isEmpty(goodsTypeList)){
           for (GoodsType goodsType : goodsTypeList) {

                getChild(goodsType);
           }
       }
        return goodsTypeList;
    }

    @Override
    public ServiceVO save(String goodsTypeName, Integer pId) {
        GoodsType goodsType = new GoodsType();
        goodsType.setText(goodsTypeName);
        Integer result= goodsTypeDao.save(goodsTypeName,pId);
        if(result>0){
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }
        return new ServiceVO(ErrorCode.SAVE_ERROR_CODE,ErrorCode.SAVE_ERROR_MESS);
    }

    @Override
    public ServiceVO delete(Integer goodsTypeId) {
        Integer result= goodsTypeDao.delete(goodsTypeId);
        if(result>0){
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }
        return new ServiceVO(ErrorCode.OPERATION_ERROR_CODE,ErrorCode.OPERATION_ERROR_MESS);
    }

    private GoodsType getChild(GoodsType goodsType) {
        List<GoodsType> goodsTypeList = goodsTypeDao.getGoodsTypeList(goodsType);
        if(CollectionUtils.isEmpty(goodsTypeList)){
            return goodsType;
        }
        List<GoodsType> collect = goodsTypeList.stream().map(goodsType1 -> getChild(goodsType1)).collect(Collectors.toList());
        String state= goodsType.getGoodsTypeState()==1?"closed":"open";
        goodsType.setState(state);
        Map<String,Object> map=new HashMap<>();
        map.put("state",goodsType.getGoodsTypeState());
        goodsType.setAttributes(map);
        goodsType.setChildren(collect);
        return goodsType;
    }

}
