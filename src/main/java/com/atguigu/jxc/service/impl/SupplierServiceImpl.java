package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    private SupplierDao supplierDao;
    @Override
    public Map<String, Object> listByPage(Integer page, Integer rows, String supplierName) {
        page=page==0?1:page;
        Integer start=(page-1)*rows;
        List<Supplier> list= supplierDao.listByPage(start,rows,supplierName);
        Map<String,Object> map= new HashMap<>();
        map.put("total",list.size());
        map.put("rows",list);
        return map;
    }

    @Override
    public Integer save(Supplier supplier) {
        //判断是添加
        if(supplier.getSupplierId()!=null){
           return supplierDao.update(supplier);
        }else {
            return  supplierDao.save(supplier);
        }
    }

    @Override
    public ServiceVO delete(String ids) {
        String[] strings = ids.split(",");
        List<String> list = Arrays.asList(strings);
        Integer delete = supplierDao.delete(list);
        if(delete>0){
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }else {
            return new ServiceVO(ErrorCode.OPERATION_ERROR_CODE,ErrorCode.OPERATION_ERROR_MESS);
        }



    }
}
