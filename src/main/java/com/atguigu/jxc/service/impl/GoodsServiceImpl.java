package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> getListInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        page=page==0?1:page;
        Integer start=(page-1)*rows;
           List<Goods> goods= goodsDao.getListInventory(start,rows,codeOrName,goodsTypeId);

           Map map=new HashMap();
           map.put("total",goods.size());
           map.put("rows",goods);
        return map;

    }
 //查询所有商品单位
    @Override
    public Map<String, Object> getUnitList() {
        List<Unit> list= goodsDao.getUnitList();
        Map<String,Object> map=new HashMap<>();
        map.put("rows",list);
        return map;
    }

    @Override
    public Map<String, Object> pageGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        page=page==0?1:page;
        Integer start=(page-1)*rows;
        Map<String,Object> map=new HashMap<>();
        List<Goods> list=goodsDao.pageGoodsList(start,rows,goodsName,goodsTypeId);
        map.put("total",list.size());
        map.put("rows",list);
        return map;
    }

    @Override
    public ServiceVO saveOrUpdate(Goods goods) {
        //设置操作状态
        Integer result;
         //判断修改
         if(goods.getGoodsId()!=null){
           result=   goodsDao.update(goods);
         }else {
             result=goodsDao.save(goods);
         }
        if(result>0){
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }
        return new ServiceVO(ErrorCode.OPERATION_ERROR_CODE,ErrorCode.OPERATION_ERROR_MESS);

    }


}
