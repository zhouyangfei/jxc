package com.atguigu.jxc.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;


public class BeanMapUtil {
    /**
     * 获取obj中的所有方法
     *
     * @param obj
     * @return
     */
    public  List<Method> getAllMethods(Object obj) {
        List<Method> methods = new ArrayList<Method>();// 获取obj字节码
        Class<?> clazz = obj.getClass();
        while (!clazz.getName().equals("java.lang.Object")){// 获取方法
            methods.addAll(Arrays.asList(clazz.getDeclaredMethods()));
            clazz = clazz.getSuperclass();
        }
        return methods;
    }

    /**
     * 将一个类用属性名为Key，值为Value的方式存入map
     *
     * @param obj
     * @return
     */
    public  Map<String, Object> convert2Map(Object obj) {
        Map<String, Object> map = new HashMap<String, Object>();
        List<Method> methods = getAllMethods(obj);
        for (Method m : methods) {
            String methodName = m.getName();
            if (methodName.startsWith("get")) {
                // 获取属性名
                String propertyName = methodName.substring(3);
                try {
                    map.put(propertyName, m.invoke(obj));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return map;
    }
}
