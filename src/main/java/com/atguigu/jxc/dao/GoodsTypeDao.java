package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 商品类别
 */
@Repository
public interface GoodsTypeDao {



    Integer updateGoodsTypeState(GoodsType parentGoodsType);



    List<GoodsType> getGoodsTypeList(GoodsType goodsType);

    Integer save(@Param("goodsTypeName") String goodsTypeName,
                 @Param("pId") Integer pId);

    Integer delete(Integer goodsTypeId);
}
