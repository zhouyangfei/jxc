package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface CustomerDao {
    List<Customer> list(@Param("page") Integer offset,
                        @Param("rows") Integer rows,
                        @Param("customerName") String customerName);

    Integer update(Customer customer);

    Integer save(Customer customer);

    Integer delete(List<String> list);
}
