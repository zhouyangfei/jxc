package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.GoodsType;

import java.util.List;

public interface GoodsTypeSerivce {
    List<GoodsType> loadGoodsType();

    ServiceVO save(String goodsTypeName, Integer pId);

    ServiceVO delete(Integer goodsTypeId);
}
