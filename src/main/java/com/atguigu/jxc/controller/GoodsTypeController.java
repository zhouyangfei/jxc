package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.service.GoodsTypeSerivce;
import com.atguigu.jxc.util.StringUtil;
import com.google.gson.Gson;
import org.apache.tomcat.util.buf.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/goodsType")
public class GoodsTypeController {
    @Autowired
    private GoodsTypeSerivce goodsTypeSerivce;

    @PostMapping("/loadGoodsType")
    public String loadGoodsType(){
        Gson gson = new Gson();
        return gson.toJson(goodsTypeSerivce.loadGoodsType());

    }
    //新增分类
    @PostMapping("/save")
    public ServiceVO save(String  goodsTypeName,Integer  pId){
      return   goodsTypeSerivce.save(goodsTypeName,pId);
    }
    //删除分类
    @PostMapping("/delete")
    public ServiceVO delete(Integer  goodsTypeId){
        return goodsTypeSerivce.delete(goodsTypeId);
    }
}
