package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Unit;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 商品信息
 */
@Repository
public interface GoodsDao  {


    String getMaxCode();


    List<Goods> getListInventory(
            @Param("page") Integer start,
            @Param("rows") Integer rows,
            @Param("codeOrName")String codeOrName,
            @Param("goodsTypeId") Integer goodsTypeId );

    List<Unit> getUnitList();

    List<Goods> pageGoodsList( @Param("page") Integer start,
                               @Param("rows") Integer rows,
                               @Param("goodsName")String goodsName,
                               @Param("goodsTypeId") Integer goodsTypeId);

    Integer update(Goods goods);

    Integer save(Goods goods);
}
