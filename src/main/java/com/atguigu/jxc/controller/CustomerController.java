package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/*
*  客户层
* */
@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;
/*客户列表分页（名称模糊查询）*/
    @PostMapping("/list")
    public Map<String,Object> list(Integer page, Integer rows, String  customerName){
        return    customerService.list(page,rows,customerName);
    }
    //客户添加或修改
    @PostMapping("/save")
    public ServiceVO saveOrUpdate(Customer customer){
        return customerService.saveOrUpdate(customer);

    }
    //客户删除
    @PostMapping("/delete")
    public ServiceVO delete(String  ids){
        return customerService.delete(ids);
    }
}
