package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import org.springframework.stereotype.Service;

import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    Map<String, Object> getListInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    Map<String, Object> getUnitList();

    Map<String, Object> pageGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    ServiceVO saveOrUpdate(Goods goods);
}
