package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDao customerDao;
    @Override
//    客户列表分页
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {

        page=page==0?1:page;
        Integer offset=(page-1)*rows;
        List<Customer> list= customerDao.list(offset,rows,customerName);
        Map<String ,Object> map=new HashMap<>();
        map.put("total",list.size());
        map.put("rows",list);
        return map;

    }

    @Override
    public ServiceVO saveOrUpdate(Customer customer) {
        Integer customerId = customer.getCustomerId();
        Integer result;
        //判断修改
        if(customerId!=null){
           result= customerDao.update(customer);
        }else {
            result=customerDao.save(customer);
        }
        if(result>0){
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }else {
            return new ServiceVO<>(ErrorCode.SAVE_ERROR_CODE,ErrorCode.SAVE_ERROR_MESS);
        }

    }

    @Override
    public ServiceVO delete(String ids) {
        String[] strings = ids.split(",");
        List<String> list = Arrays.asList(strings);
        Integer delete = customerDao.delete(list);
        if(delete>0){
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }else {
            return new ServiceVO(ErrorCode.OPERATION_ERROR_CODE,ErrorCode.OPERATION_ERROR_MESS);
        }
    }
}
